import styles from "./styles.module.scss";
import classnames from "classnames";

export default function Row({ title, subtitle, className, children }) {
  return (
    <div
      className={classnames({
        [styles.row]: className === undefined,
        [styles.rowFocus]: className === "focus",
        [styles.rowInactive]: className === "inactive",
        [styles.rowInactiveLight]: className === "inactiveLight",
        [styles.rowFocusLight]: className === "focusLight",
        [styles.rowLight]: className === "unfocussedLight",
      })}
    >
      <div className={classnames(styles.cell, styles.desc)}>
        <div>{title}</div>
        <div>{subtitle}</div>
      </div>
      {children.map((Child, index) => (
        <div key={`child_${index}`} className={styles.cell}>
          {Child}
        </div>
      ))}
    </div>
  );
}
