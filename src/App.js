import {
  Face,
  Cast,
  Chat,
  Wifi,
  LocationOn,
  FaceOutlined,
  CastOutlined,
  ChatOutlined,
  WifiOutlined,
  LocationOnOutlined,
  FaceRounded,
  ChatRounded,
  WifiRounded,
  LocationOnRounded,
  FaceTwoTone,
  CastRounded,
  CastTwoTone,
  ChatTwoTone,
  WifiTwoTone,
  LocationOnTwoTone,
  FaceSharp,
  CastSharp,
  ChatSharp,
  WifiSharp,
  LocationOnSharp,
  SettingsBackupRestore,
  AccessAlarm,
  AccessAlarmOutlined,
  Description,
  DescriptionOutlined,
  DescriptionRounded,
  DescriptionTwoTone,
  DescriptionSharp,
  AccessAlarmRounded,
  AccessAlarmTwoTone,
  AccessAlarmSharp,
} from "@material-ui/icons";
import Row from "./Row";
import styles from "./styles.module.scss";
const iconComponents = [
  Face,
  Cast,
  Chat,
  Wifi,
  LocationOn,
  AccessAlarm,
  Description,
];

const Content = ({ children }) => (
  <div className={styles.content}>{children}</div>
);

const Container = ({ children }) => (
  <div className={styles.container}>{children}</div>
);

const colorProps = [
  { color: "primary" },
  { color: "secondary" },
  { color: "action" },
  { color: "disabled" },
  { color: "error" },
];

function App() {
  const icons = (getProps, components) => {
    components = components || iconComponents;
    return components.map((Icon, index) => {
      const props = getProps ? getProps(index) : {};
      return <Icon key={`icon_${index}`} {...props} />;
    });
  };
  return (
    <Content>
      <Container>
        <Row className="focus" title="Focussed" subtitle="black, 87%">
          {icons()}
        </Row>
        <Row title="Unfocussed" subtitle="black, 54%">
          {icons()}
        </Row>
        <Row className="inactive" title="Inactive" subtitle="black, 38%">
          {icons()}
        </Row>
      </Container>
      <Container>
        <Row className="focusLight" title="Focussed" subtitle="white, 100%">
          {icons()}
        </Row>
        <Row
          className="unfocussedLight"
          title="Unfocussed"
          subtitle="white, 70%"
        >
          {icons()}
        </Row>
        <Row className="inactiveLight" title="Inactive" subtitle="white, 50%">
          {icons()}
        </Row>
      </Container>
      <Container>
        <Row title="Colors" subtitle="primary, secondary, active, disabled">
          {icons((i) => colorProps[i], [
            Description,
            Description,
            Description,
            Description,
            Description,
          ])}
        </Row>
      </Container>
      <Container>
        <Row title="Rotation" subtitle="transform: rotate(45deg)">
          {icons(
            (i) =>
              [
                {},
                { className: styles.rotate1 },
                { className: styles.rotate2 },
                { className: styles.rotate3 },
              ][i],
            [Face, Face, Face, SettingsBackupRestore]
          )}
        </Row>
      </Container>
      <Container>
        <Row title="Sizing" subtitle="18px, 24px, 36px, 48px">
          {icons(
            (i) =>
              [
                { style: { fontSize: 18 } },
                { style: { fontSize: 24 } },
                { style: { fontSize: 36 } },
                { style: { fontSize: 48 } },
              ][i],
            [Face, Face, Face, Face]
          )}
        </Row>
      </Container>
      <Container>
        <Row title="Regular">{icons()}</Row>
        <Row title="Outlined" subtitle="no fills">
          {icons(null, [
            FaceOutlined,
            CastOutlined,
            ChatOutlined,
            WifiOutlined,
            LocationOnOutlined,
            AccessAlarmOutlined,
            DescriptionOutlined,
          ])}
        </Row>
        <Row title="Rounded" subtitle="rounded corners">
          {icons(null, [
            FaceRounded,
            CastRounded,
            ChatRounded,
            WifiRounded,
            LocationOnRounded,
            AccessAlarmRounded,
            DescriptionRounded,
          ])}
        </Row>
        <Row title="Two-Tone" subtitle="two colors where possible">
          {icons((i) => (i === 4 ? { color: "secondary" } : {}), [
            FaceTwoTone,
            CastTwoTone,
            ChatTwoTone,
            WifiTwoTone,
            LocationOnTwoTone,
            AccessAlarmTwoTone,
            DescriptionTwoTone,
          ])}
        </Row>
        <Row title="Sharp" subtitle="sharp corners">
          {icons(null, [
            FaceSharp,
            CastSharp,
            ChatSharp,
            WifiSharp,
            LocationOnSharp,
            AccessAlarmSharp,
            DescriptionSharp,
          ])}
        </Row>
      </Container>
    </Content>
  );
}

export default App;
